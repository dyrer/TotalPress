<?php /* @version 1.0.24 */
if ( ! defined('ABSPATH')) exit;
get_header(); ?>
<?php do_action('totalpress_404_start') ;?>
<?php do_action('totalpress_404_entry_content') ;?>
<?php do_action('totalpress_404_end') ;?>
<?php
get_footer();